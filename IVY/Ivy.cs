﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using IVY.Objets;

namespace IVY
{
    /// <summary>
    /// La classe Ivy contrôle le programme et ses événements.
    /// </summary>
    public class Ivy
    {
        /// <summary>
        /// Représente le client grâce auquel le programme pourra communiquer 
        /// avec Discord (WebSocket).
        /// </summary>
        public DiscordSocketClient Client;

        /// <summary>
        /// Appelle la classe Membre.
        /// </summary>
        public readonly Membre Membres;

        /// <summary>
        /// Appelle la classe Message.
        /// </summary>
        public readonly Message Messages;

        /// <summary>
        /// Appelle la classe Pointage.
        /// </summary>
        public readonly Pointage Pointages;

        /// <summary>
        /// Appelle la classe Reaction.
        /// </summary>
        public readonly Reaction Reactions;

        /// <summary>
        /// Constructeur de la classe Ivy. Configure le DiscordSocketClient et 
        /// gère les événements Discord.
        /// </summary>
        /// <param name="token">Le token du bot associé au programme.</param>
        public Ivy(string token)
        {
            // On initialise le nouveau client Discord avec la
            // configuration donnée.
            //
            // (DiscordSocketConfig possède déjà des configurations par défaut:
            // nous n'ajoutons que celles que nous voulons de plus.)
            Client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                // Nous nous assurons ici que les utilisateurs seront toujours
                // téléchargés automatiquement.
                AlwaysDownloadUsers = true
            });

            Membres = new();
            Messages = new();
            Pointages = new();
            Reactions = new();

            // Fonction appelée à l'entrée d'un nouveau log, et lance la
            // méthode les affichant en console.
            Client.Log += Log;

            // Fonction appelée dès qu'une guilde est disponible, et lance la
            // méthode de préparation du pointage des membres de celle-ci.
            //
            // (Le programme n'est fait que pour être utilisé sur un serveur à
            // la fois: cette fonction ne marchera pas sinon.)
            Client.GuildAvailable += Initialisation;

            // Fonction appelée dès qu'une réaction est ajoutée à un message,
            // et lance la méthode d'analyse.
            Client.ReactionAdded += ReactionDetectee;

            // Fonction appelée quand un message est supprimé afin de le
            // retirer du répertoire des messages.
            Client.MessageDeleted += SuppressionMessage;

            // Fonction appelée quand un membre est banni de la guilde afin de
            // le retirer du répertoire des membres.
            Client.UserBanned += BannissementMembre;

            try
            {
                // Permet au programme de se connecter en tant que bot selon le
                // token donné avec validation.
                TokenUtils.ValidateToken(TokenType.Bot, token);
                Client.LoginAsync(TokenType.Bot, token);
            }
            catch (ArgumentException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Sort toutes les informations dans les répertoires sur le membre 
        /// banni de la guilde.
        /// </summary>
        /// <param name="membre">Le membre banni.</param>
        /// <param name="guilde">La guilde d'où le membre est banni.</param>
        /// <returns>La tâche complétée.</returns>
        private Task BannissementMembre(SocketUser membre, SocketGuild guilde)
        {
            guilde.DefaultChannel.SendMessageAsync(
                $"{membre.Mention} a été banni pour mauvaise conduite. " +
                $"Au nom des responsables de la communauté {guilde.Name}, " +
                $"merci de garder une attitude positive! ❤️");

            IGuildUser membreBanni = Membres.ObtenirMembre(membre.Id);
            if (membreBanni == null && !membre.IsBot && !membre.IsWebhook)
            {
                Console.WriteLine($"Membre ajouté {membre.Username}");
                IGuildUser nouveauMembre = membre as IGuildUser;
                Membres.AjouterMembre(nouveauMembre);
                Pointages.AjouterMembrePointage(nouveauMembre);
                Messages.AjouterNouveauMembreMessages(nouveauMembre.Id);
            }
            Pointages.RetirerMembrePointage(membre.Id);
            Messages.ViderMessagesChanceGagnee(membre.Id);
            Messages.ViderMessagesChancePerdue(membre.Id);
            Messages.RetirerMembreMessages(membre.Id);
            Membres.RetirerMembre(Membres.ObtenirMembre(membre.Id));
            return Task.CompletedTask;
        }

        /// <summary>
        /// Permet au programme de se lancer.
        /// (Pour le lancement depuis les tests.)
        /// </summary>
        public void Lancement()
        {
            Client.StartAsync();
        }

        /// <summary>
        /// Permet au programme de se lancer avec .Wait().
        /// (Pour le lancement normal depuis Program.cs.)
        /// </summary>
        public void LancementEnAttente()
        {
            Client.StartAsync().Wait();
        }

        /// <summary>
        /// Permet d'afficher textuellement les logs du programme en console.
        /// (Sert de substitut rudimentaire à un logging framework.)
        /// </summary>
        /// <param name="msg">Le log entrant à afficher en console.</param>
        /// <returns>La tâche complétée.</returns>
        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        /// <summary>
        /// Lorsqu'un utilisateur retire un message, on s'assure qui soit 
        /// retiré des listes des chances gagnées/perdues si nécessaire.
        /// </summary>
        /// <param name="messageSupprime">Le message supprimé.</param>
        /// <param name="salon">
        /// Le salon du message supprimé. (Requis pour Client.MessageDeleted.)
        /// </param>
        /// <returns>La tâche complétée.</returns>
        private async Task<Task> SuppressionMessage(
                                    Cacheable<IMessage, ulong> messageSupprime,
                                    Cacheable<IMessageChannel, ulong> salon)
        {
            var message = await messageSupprime.GetOrDownloadAsync();

            if (message != null)
            {
                ulong auteurId = message.Author.Id;
                IGuildUser auteur = Membres.ObtenirMembre(auteurId);

                if (auteur == null && !message.Author.IsBot && !message.Author.IsWebhook)
                {
                    Console.WriteLine($"Membre ajouté {message.Author.Username}");
                    IGuildUser nouveauMembre = message.Author as IGuildUser;
                    Membres.AjouterMembre(nouveauMembre);
                    Pointages.AjouterMembrePointage(nouveauMembre);
                    Messages.AjouterNouveauMembreMessages(nouveauMembre.Id);
                    auteur = Membres.ObtenirMembre(nouveauMembre.Id);
                }

                // On vérifie que le message vient d'un utilisateur faisant partie
                // du système de pointage avant de poursuivre.
                if (message != null && Pointages.EstDansSystemePointage(auteurId))
                {
                    //Si la liste n'est pas nulle, on vérifie si le message a fait
                    //gagner une chance à son auteur dans le passé et on le retire
                    //de la liste.
                    if (Messages.ObtenirMessagesChanceGagnee(auteurId) != null)
                    {
                        foreach (var msgPositif in
                           Messages.ObtenirMessagesChancePerdue(auteurId))
                        {
                            if (message.Id == msgPositif.Id)
                            {
                                Messages.RetirerMessageChanceGagnee(msgPositif);
                            }
                        }
                    }
                    // Si la liste n'est pas nulle, on vérifie si le message a fait
                    // perdre une chance à son auteur dans le passé et le retire de
                    // la liste.
                    if (Messages.ObtenirMessagesChancePerdue(auteurId) != null)
                    {
                        foreach (var msgNegatif in
                           Messages.ObtenirMessagesChancePerdue(auteurId))
                        {
                            if (message.Id == msgNegatif.Id)
                            {
                                Messages.RetirerMessageChancePerdue(msgNegatif);
                            }
                        }
                    }
                }
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Boucle sur tous les membres de la guilde donnée afin d'ajouter les 
        /// membres "normaux" au système de pointage.
        /// (Le pointage initiale est de 5 chances.)
        /// </summary>
        /// <param name="guilde">La guilde à laquel appartient le bot.</param>
        /// <returns>La tâche complétée.</returns>
        private async Task<Task> Initialisation(SocketGuild guilde)
        {
            await foreach (var membres in guilde.GetUsersAsync())
            {
                // On ne veut pas ajouter le propriétaire de la guilde au
                // pointage, ni les webhooks et les bots.
                var membresEligibles = membres.Where(
                    m => !m.IsBot && !m.IsWebhook && m.Id != guilde.OwnerId);

                // Si la liste contient des membres éligibles, on les ajoutes à
                // la liste des membres modérés.
                if (membresEligibles.Any())
                {
                    foreach (var membre in membresEligibles)
                    {
                        Membres.AjouterMembre(membre);
                        Pointages.AjouterMembrePointage(membre);
                        Messages.AjouterNouveauMembreMessages(membre.Id);
                    }
                }
            }

            // Si le répertoire des membres à modérer est vide, on lance
            // l'avertissement.
            if (!Membres.ObtenirMembres().Any())
            {
                await guilde.DefaultChannel.SendMessageAsync("**Attention: " +
                    "aucun membre à modérer est actuellement détecté!**\n" +
                    "(Rappel: le propriétaire de la guilde, les webhooks " +
                    "ainsi que les bots ne sont pas modérés.)");
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// La méthode permet de faire l'analyse de la réaction et déterminer 
        /// si elle a un impacte ou non sur le pointage de l'auteur du message.
        /// </summary>
        /// <param name="messageVise">Le message visé par la réaction.</param>
        /// <param name="salonVise">
        /// Le salon textuel du message visé par une nouvelle réaction. 
        /// (Nécessaire pour la fonction Client.ReactionAdded.)
        /// </param>
        /// <param name="reaction">La nouvelle réaction détectée.</param>
        /// <returns>La tâche complétée.</returns>
        private async Task<Task> ReactionDetectee(
                                   Cacheable<IUserMessage, ulong> messageVise, 
                                   Cacheable<IMessageChannel, ulong> salonVise, 
                                   SocketReaction reaction)       
        {
            var message = await messageVise.GetOrDownloadAsync();
            // Si le message n'est pas null (comme dans l'éventualité d'un
            // message d'ajout d'un nouveau membre).
            if (message != null)
            {
                ulong auteurId = (await messageVise.GetOrDownloadAsync()).Author.Id;
                IGuildUser auteur = Membres.ObtenirMembre(auteurId);

                if (auteur == null && 
                    !(await messageVise.GetOrDownloadAsync()).Author.IsBot && 
                    !(await messageVise.GetOrDownloadAsync()).Author.IsWebhook)
                {
                    Console.WriteLine($"Membre ajouté {(await messageVise.GetOrDownloadAsync()).Author.Username}");
                    IGuildUser nouveauMembre = (await messageVise.GetOrDownloadAsync()).Author as IGuildUser;
                    Membres.AjouterMembre(nouveauMembre);
                    Pointages.AjouterMembrePointage(nouveauMembre);
                    Messages.AjouterNouveauMembreMessages(nouveauMembre.Id);
                    auteur = Membres.ObtenirMembre(nouveauMembre.Id);
                }

                // Si l'auteur du message fait partie du système de pointage et
                // n'est pas en exclusion.
                if (Pointages.EstDansSystemePointage(auteurId) && 
                    auteur.TimedOutUntil.HasValue.Equals(false))
                {
                    // S'il s'agit d'une appréciation et que le message n'a pas
                    // déjà affecté les gains de chances de l'auteur.
                    if (Reactions.EstAppreciation(reaction.Emote.Name) && 
                        !(Messages.EstMessageChanceGagnee(message)))
                    {
                        int nbAppreciations = 
                            await message
                            .GetReactionUsersAsync(
                                Reactions.ObtenirAppreciation(), 5)
                            .CountAsync();
                        if (nbAppreciations == 5)
                        {
                            Pointages.AugmenterPointage(auteurId);
                            Messages.AjouterMessageChanceGagnee(message);
                        }
                    }
                    // S'il s'agit d'une plainte et que le message n'a pas déjà
                    // affecté les pertes de chances de l'auteur.
                    else if (Reactions.EstPlainte(reaction.Emote.Name) && 
                        !(Messages.EstMessageChancePerdue(message)))
                    {
                        int nbPlaintes = await message
                                        .GetReactionUsersAsync(
                                                 Reactions.ObtenirPlainte(), 5)
                                        .CountAsync();
                        if (nbPlaintes == 5)
                        {
                            Pointages.DiminuerPointage(auteurId);
                            Messages.AjouterMessageChancePerdue(message);
                            if (Pointages.ObtenirPointage(auteurId) == 0)
                            {
                                await Suspension(auteur, salonVise.Value);
                            }
                        }
                    }
                }
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Suspends l'utilisateur problématique et nettoie les listes et les 
        /// messages causant problèmes.
        /// </summary>
        /// <param name="suspendu">L'utilisateur à suspendre.</param>
        /// <param name="salonVise">Le dernier salon accédé par le suspendu.</param>
        /// <returns>La tâche complétée.</returns>
        public Task Suspension(IGuildUser suspendu, IMessageChannel salonVise)
        {
            if (Membres.ObtenirMembres() == null && !suspendu.IsBot && !suspendu.IsWebhook)
            {
                Console.WriteLine($"Membre ajouté {suspendu.Username}");
                Membres.AjouterMembre(suspendu);
                Pointages.AjouterMembrePointage(suspendu);
                Messages.AjouterNouveauMembreMessages(suspendu.Id);
            }

            // Si l'utilisateur a des messages encore problématiques.
            // (Il pourrait en avoir supprimé...)
            if (Messages.ObtenirMessagesChancePerdue(suspendu.Id) != null)
            {
                // On supprime tous les messages problématiques de
                // l'utilisateur.
                foreach (var message in 
                        Messages.ObtenirMessagesChancePerdue(suspendu.Id))
                {
                    // Si le message existe aussi dans les chances obtenues,
                    // on le retire.
                    if (Messages.ObtenirMessagesChanceGagnee(suspendu.Id)
                        .Contains(message))
                    {
                        Messages.RetirerMessageChanceGagnee(message);
                    }
                    message.DeleteAsync();
                }
                // On vide finalement la liste des messages problématiques de
                // l'auteur qui ont été détectés par le programme depuis sont
                // lancement.
                Messages.ViderMessagesChancePerdue(suspendu.Id);
            }

            // On suspend l'utilisateur puis on lui donne une chance.
            Membres.SuspendreMembre(suspendu.Id);
            Pointages.AugmenterPointage(suspendu.Id);
            salonVise.SendMessageAsync(
                $"{suspendu.Mention} a été suspendu pour mauvaise conduite. " +
                $"N'oubliez pas qu'une bonne attitude est nécessaire pour le " +
                $"bien de la communauté! ❤️");
            return Task.CompletedTask;
        }
    }
}
