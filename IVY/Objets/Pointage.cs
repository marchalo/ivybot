﻿using Discord;
using IVY.Repertoires;

namespace IVY.Objets
{
    /// <summary>
    /// La classe Pointage s'occupe de la gestion du système de pointage.
    /// </summary>
    public class Pointage: RepertoirePointages
    {
        /// <summary>
        /// Constructeur de la classe Pointage.
        /// </summary>
        public Pointage() { }

        /// <summary>
        /// Permet d'obtenir le pointage d'un membre donné.
        /// </summary>
        /// <param name="membreId">L'id du membre dont on cherche le pointage.</param>
        /// <returns>Le pointage du membre donné.</returns>
        public int ObtenirPointage(ulong membreId)
        {
            return Pointages[membreId];
        }

        /// <summary>
        /// Permet de déterminer si le membre fait partie du système de pointage.
        /// </summary>
        /// <param name="membreId">L'id du membre à vérifier.</param>
        /// <returns>True si le membre est dans le pointage, false sinon.</returns>
        public bool EstDansSystemePointage(ulong membreId)
        {
            return (Pointages.ContainsKey(membreId));
        }

        /// <summary>
        /// Permet d'ajouter un nouveau membre au système de pointage.
        /// </summary>
        /// <param name="membre">Le membre à ajouter.</param>
        public void AjouterMembrePointage(IGuildUser membre)
        {
            // On commence toujours avec 5 chances.
            int chances = 5;
            // Si le membre est déjà suspendu, on lui donne qu'une chance.
            if (membre.TimedOutUntil.HasValue)
            {
                chances = 1;
            }
            Pointages.Add(membre.Id, chances);
        }

        /// <summary>
        /// Permet de retirer un membre du système de pointage.
        /// </summary>
        /// <param name="membreId">L'id du membre à retirer.</param>
        public void RetirerMembrePointage(ulong membreId)
        {
            Pointages.Remove(membreId);
        }

        /// <summary>
        /// Permet d'ajouter une chance au pointage d'un membre donné.
        /// </summary>
        /// <param name="membreId">L'id du membre dont le pointage augmente.</param>
        public void AugmenterPointage(ulong membreId)
        {
            Pointages[membreId] += 1;
        }

        /// <summary>
        /// Permet de retirer une chance au pointage d'un membre donné.
        /// </summary>
        /// <param name="membreId">L'id du membre dont le pointage diminue.</param>
        public void DiminuerPointage(ulong membreId)
        {
            Pointages[membreId] -= 1;
        }
    }
}
