﻿using Discord;
using System.Collections.Generic;
using IVY.Repertoires;

namespace IVY.Objets
{
    /// <summary>
    /// S'occupe de la gestion d'un message par la modération.
    /// </summary>
    public class Message: RepertoireMessages
    {
        /// <summary>
        /// Constructeur de la classe Message.
        /// </summary>
        public Message() { }

        /// <summary>
        /// Permet d'ajouter un nouveau message à la liste de ceux ayant fait 
        /// gagner une chance à leur auteur.
        /// </summary>
        /// <param name="message">Le message à ajouter à la liste.</param>
        public void AjouterMessageChanceGagnee(IUserMessage message)
        {
            MessagesChanceGagnee[message.Author.Id].Add(message);
        }

        /// <summary>
        /// Permet d'ajouter un nouveau message à la liste de ceux ayant fait 
        /// perdre une chance à leur auteur.
        /// </summary>
        /// <param name="message">Le message à ajouter à la liste.</param>
        public void AjouterMessageChancePerdue(IUserMessage message)
        {
            MessagesChancePerdue[message.Author.Id].Add(message);
        }

        /// <summary>
        /// Permet de retirer un message de la liste des messages ayant fait 
        /// gagner une chance de son auteur.
        /// </summary>
        /// <param name="message">Le message à retirer.</param>
        public void RetirerMessageChanceGagnee(IUserMessage message)
        {
            MessagesChanceGagnee[message.Author.Id].Remove(message);
        }

        /// <summary>
        /// Permet de retirer un message de la liste des messages ayant fait 
        /// perdre une chance de son auteur.
        /// </summary>
        /// <param name="message">Le message à retirer.</param>
        public void RetirerMessageChancePerdue(IUserMessage message)
        {
            MessagesChancePerdue[message.Author.Id].Remove(message);
        }

        /// <summary>
        /// Permet d'ajouter un nouveau membre aux listes des messages 
        /// affectants les chances de leur auteur.
        /// On commence toujours avec des listes vides.
        /// </summary>
        /// <param name="membreId">L'id du membre à ajouter aux listes.</param>
        public void AjouterNouveauMembreMessages(ulong membreId)
        {
            MessagesChanceGagnee.Add(membreId, new List<IUserMessage>());
            MessagesChancePerdue.Add(membreId, new List<IUserMessage>());
        }

        /// <summary>
        /// Permet de retirer un membre aux listes des messages affectants les 
        /// chances de leur auteur.
        /// </summary>
        /// <param name="membreId">L'id du membre à retirer des listes.</param>
        public void RetirerMembreMessages(ulong membreId)
        {
            MessagesChanceGagnee.Remove(membreId);
            MessagesChancePerdue.Remove(membreId);
        }

        /// <summary>
        /// Permet de déterminer si le message donné est déjà dans la liste des 
        /// messages ayant fait gagner une chance à son auteur.
        /// </summary>
        /// <param name="message">Le message à vérifier.</param>
        /// <returns>True si le message est dans la liste, false sinon.</returns>
        public bool EstMessageChanceGagnee(IUserMessage message)
        {
            return (ObtenirMessagesChanceGagnee(message.Author.Id) != null && 
                    MessagesChanceGagnee[message.Author.Id].Contains(message));
        }

        /// <summary>
        /// Permet de déterminer si le message donné est déjà dans la liste des 
        /// messages ayant fait perdre une chance à son auteur.
        /// </summary>
        /// <param name="message">Le message à vérifier.</param>
        /// <returns>True si le message est dans la liste, false sinon.</returns>
        public bool EstMessageChancePerdue(IUserMessage message)
        {
            return (ObtenirMessagesChancePerdue(message.Author.Id) != null && 
                    MessagesChancePerdue[message.Author.Id].Contains(message));
        }
    }
}
