﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVYTests
{
    /// <summary>
    /// Classe de tests concernant les réactions.
    /// </summary>
    [TestClass]
    public class TestsReactions
    {
        #region Tests sur EstAppreciation()

        /// <summary>
        /// Teste que la méthode EstAppreciation() retourne true lorsqu'on lui
        /// fournie le nom valide de l'emoji d'appréciation.
        /// </summary>
        [TestMethod]
        public void TestReactionEstAppreciationSucces()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                Assert.IsTrue(bot.Reactions.EstAppreciation("❤️"));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la méthode EstAppreciation() retourne false lorsqu'on lui
        /// fournie un mauvais nom d'emoji, comme celui de plainte par exemple.
        /// </summary>
        [TestMethod]
        public void TestReactionEstAppreciationEchec()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                Assert.IsFalse(bot.Reactions.EstAppreciation("🚩"));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la méthode EstAppreciation() retourne false quand elle
        /// reçoit une valeur nulle.
        /// </summary>
        [TestMethod]
        public void TestReactionEstAppreciationNulle()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                Assert.IsFalse(bot.Reactions.EstAppreciation(null));
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur EstPlainte()
        /// <summary>
        /// Teste que la méthode EstPlainte() retourne true lorsqu'on lui
        /// fournie le nom valide de l'emoji de plainte.
        /// </summary>
        [TestMethod]
        public void TestReactionEstPlainteSucces()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                Assert.IsTrue(bot.Reactions.EstPlainte("🚩"));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la méthode EstPlainte() retourne false lorsqu'on lui fournie
        /// un mauvais nom d'emoji, comme celui d'appréciation par exemple.
        /// </summary>
        [TestMethod]
        public void TestReactionEstPlainteEchec()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);

                Assert.IsFalse(bot.Reactions.EstPlainte("❤️"));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la méthode EstPlainte() retourne false quand elle
        /// reçoit une valeur nulle.
        /// </summary>
        [TestMethod]
        public void TestReactionEstPlainteNulle()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                Assert.IsFalse(bot.Reactions.EstPlainte(null));
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur ObtenirAppreciation()

        /// <summary>
        /// Teste que la méthode ObtenirAppreciation() retourne le bon emoji.
        /// </summary>
        [TestMethod]
        public void TestReactionObtenirAppreciationSucces()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                Assert.IsTrue(bot.Reactions.ObtenirAppreciation().Name == "❤️");
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la méthode ObtenirAppreciation() ne retourne pas un
        /// emoji autre que celui considéré comme une appréciation. ("❤️")
        /// </summary>
        [TestMethod]
        public void TestReactionObtenirAppreciationEchec()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                Assert.IsFalse(bot.Reactions.ObtenirAppreciation().Name != "❤️");
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la méthode ObtenirAppreciation() ne retourne pas une
        /// valeur nulle.
        /// </summary>
        [TestMethod]
        public void TestReactionObtenirAppreciationNulle()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                Assert.IsFalse(bot.Reactions.ObtenirAppreciation() == null);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur ObtenirPlainte()

        /// <summary>
        /// Teste que la méthode ObtenirPlainte() retourne le bon emoji.
        /// </summary>
        [TestMethod]
        public void TestReactionObtenirPlainteSucces()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                Assert.IsTrue(bot.Reactions.ObtenirPlainte().Name == "🚩");
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la méthode ObtenirPlainte() ne retourne pas une
        /// valeur nulle.
        /// </summary>
        [TestMethod]
        public void TestReactionObtenirPlainteNulle()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                Assert.IsFalse(bot.Reactions.ObtenirPlainte() == null);
            }
            catch (ArgumentException) { }
        }

        #endregion
    }
}
