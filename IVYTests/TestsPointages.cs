﻿using Discord;
using IVY.Objets;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace IVYTests
{
    /// <summary>
    /// Classe des tests concernant les pointages. 
    /// </summary>
    [TestClass]
    public class TestsPointages
    {
        #region Tests sur ObtenirPointages() 

        /// <summary>
        /// Teste que le pointage contient des entrées, c'est-à-dire des 
        /// membres aptes à la modération, une fois le programme bien démarré,
        /// soit avec un décallage de quelques secondes.
        /// (Devrait être complété donc .IsTrue())
        /// </summary>
        [TestMethod]
        public void TestObtenirPointagesApresLancementSucces()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                bot.Lancement();
                // On donne un délai de 10 secondes pour charger les pointages.
                Task.Delay(10000).Wait();
                Assert.IsTrue(bot.Pointages.ObtenirPointages().Count > 0);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que le pointage contient des entrées, c'est-à-dire des 
        /// membres aptes à la modération, immédiatement au démarrage.
        /// (Devrait être encore vide donc .IsFalse())
        /// </summary>
        [TestMethod]
        public void TestObtenirPointagesAuLancementEchec()
        {
            try
            {
                StreamReader sr = new("C:\\TokenBot.txt");
                var token = sr.ReadLine();
                IVY.Ivy bot = new(token);
                bot.Lancement();
                Assert.IsFalse(bot.Pointages.ObtenirPointages().Count > 0);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur ObtenirPointage()

        /// <summary>
        /// Teste que la méthode ObtenirPointage() retourne le bon
        /// pointage pour l'utilisateur donné.
        /// </summary>
        [TestMethod]
        public void TestObtenirPointageSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Pointage pointage = new();
                pointage.AjouterMembrePointage(utilisateur);
                pointage.AugmenterPointage(utilisateur.Id);
                // 5pts de départ +1pt devrait retourner 6pts
                Assert.IsTrue(pointage.ObtenirPointage(utilisateur.Id) == 6);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de ObtenirPointage() lorsqu'on lui
        /// envoie un utilisateur absent de la liste.
        /// </summary>
        [TestMethod]
        public void TestObtenirPointageEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Pointage pointage = new();
                Action action = () => pointage.ObtenirPointage(utilisateur.Id);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur EstDansSystemePointage()

        /// <summary>
        /// Teste que la méthode EstDansSystemePointage() retourne
        /// true quand un utilisateur est dans le système de pointage.
        /// </summary>
        [TestMethod]
        public void TestEstDansSystemePointageSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Pointage pointage = new();
                pointage.AjouterMembrePointage(utilisateur);
                Assert.IsTrue(pointage.EstDansSystemePointage(utilisateur.Id));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la méthode EstDansSystemePointage() retourne
        /// false quand un utilisateur n'est pas dans le système de pointage.
        /// </summary>
        [TestMethod]
        public void TestEstDansSystemePointageEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Pointage pointage = new();
                Assert.IsFalse(pointage.EstDansSystemePointage(utilisateur.Id));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode EstDansSystemePointage() 
        /// lorsqu'on lui envoie une valeur nulle.
        /// </summary>
        [TestMethod]
        public void TestEstDansSystemePointageNull()
        {
            try
            {
                Pointage pointage = new();
                int? x = null;
                Action action = () => pointage.EstDansSystemePointage((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur AjouterMembrePointage()

        /// <summary>
        /// Teste que la méthode AjouteMembrePointage() ajoute bel et bien un
        /// membre non-suspendu au répertoire des pointages et ce, en lui donnant
        /// initialement 5 points.
        /// </summary>
        [TestMethod]
        public void TestAjouterMembrePointageNonSuspendu()
        {
            try
            {
                Pointage pointage = new();
                IGuildUser membreTest = new Mock<IGuildUser>().Object;
                pointage.AjouterMembrePointage(membreTest);
                // Un membre qui n'est pas suspendu débute à 5 points.
                Assert.IsTrue(pointage.ObtenirPointage(membreTest.Id) == 5);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la méthode AjouterMembrePointage() ajoute bel et bien un
        /// membre suspendu au répertoire des pointages et ce, en lui donnant 
        /// qu'un seul point.
        /// </summary>
        [TestMethod]
        public void TestAjouterMembrePointageSuspendu()
        {
            try
            {
                Pointage pointage = new();
                IGuildUser membreTest = new Mock<IGuildUser>((DateTime.Now.AddDays(1)) - DateTime.Now).Object;
                pointage.AjouterMembrePointage(membreTest);
                // Un membre suspendu débute avec un seul point.
                Assert.IsTrue(pointage.ObtenirPointage(membreTest.Id) == 1);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste la méthode AjouterMembrePointage() lorsqu'on lui
        /// envoie une entrée nulle en paramètre.
        /// </summary>
        [TestMethod]
        public void TestAjouterMembrePointageNull()
        {
            try
            {
                Pointage pointage = new();
                Action action = () => pointage.AjouterMembrePointage(null);
                Assert.ThrowsException<NullReferenceException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur RetirerMembrePointage()

        /// <summary>
        /// Teste la méthode RetirerMembrePointage() lorsqu'on lui
        /// demande de retirer un utilisateur valide du système.
        /// </summary>
        [TestMethod]
        public void TestRetirerMembrePointageSucces()
        {
            try
            {
                IGuildUser utilisateur1 = new Mock<IGuildUser>().Object;
                IGuildUser utilisateur2 = new Mock<IGuildUser>().Object;
                IGuildUser utilisateur3 = new Mock<IGuildUser>().Object;
                Pointage pointage = new();
                pointage.AjouterMembrePointage(utilisateur1);
                pointage.AjouterMembrePointage(utilisateur2);
                pointage.AjouterMembrePointage(utilisateur3);
                Assert.IsTrue(pointage.ObtenirPointages().Count == 3);
                pointage.RetirerMembrePointage(utilisateur2.Id);
                Assert.IsTrue(pointage.ObtenirPointages().Count == 2 && 
                              !pointage.EstDansSystemePointage(utilisateur2.Id));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste la méthode RetirerMembrePointage() lorsqu'on lui
        /// envoie une entrée nulle en paramètre.
        /// </summary>
        [TestMethod]
        public void TestRetirerMembrePointageNull()
        {
            try
            {
                Pointage pointage = new();
                int? x = null;
                Action action = () => pointage.RetirerMembrePointage((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur AugmenterPointage()

        /// <summary>
        /// Teste la méthode AugmenterPointage() lorsqu'on lui
        /// envoie un utilisateur membre du système en entrée.
        /// </summary>
        [TestMethod]
        public void TestAugmenterPointageSucces()
        {
            try
            {
                Pointage pointage = new();
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                pointage.AjouterMembrePointage(utilisateur);
                int pt = pointage.ObtenirPointage(utilisateur.Id);
                pointage.AugmenterPointage(utilisateur.Id);
                int reponse = pt + 1;
                Assert.IsTrue(pointage.ObtenirPointage(utilisateur.Id) == reponse);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste la méthode AugmenterPointage() lorsqu'on lui
        /// envoie un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestAugmenterPointageEchec()
        {
            try
            {
                Pointage pointage = new();
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Action action = () => pointage.AugmenterPointage(utilisateur.Id);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste la méthode AugmenterPointage() lorsqu'on lui
        /// envoie une entrée nulle en paramètre.
        /// </summary>
        [TestMethod]
        public void TestAugmenterPointageNull()
        {
            try
            {
                Pointage pointage = new();
                int? x = null;
                Action action = () => pointage.AugmenterPointage((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur DiminuerPointage()

        /// <summary>
        /// Teste la méthode DiminuerPointage() lorsqu'on lui
        /// envoie un utilisateur membre du système en entrée.
        /// </summary>
        [TestMethod]
        public void TestDiminuerPointageSucces()
        {
            try
            {
                Pointage pointage = new();
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                pointage.AjouterMembrePointage(utilisateur);
                int pt = pointage.ObtenirPointage(utilisateur.Id);
                pointage.DiminuerPointage(utilisateur.Id);
                int reponse = pt - 1;
                Assert.IsTrue(pointage.ObtenirPointage(utilisateur.Id) == reponse);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste la méthode DiminuerPointage() lorsqu'on lui
        /// envoie un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestDiminuerPointageEchec()
        {
            try
            {
                Pointage pointage = new();
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Action action = () => pointage.DiminuerPointage(utilisateur.Id);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste la méthode DiminuerPointage() lorsqu'on lui
        /// envoie une entrée nulle en paramètre.
        /// </summary>
        [TestMethod]
        public void TestDiminuerPointageNull()
        {
            try
            {
                Pointage pointage = new();
                int? x = null;
                Action action = () => pointage.DiminuerPointage((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion
    }
}
