﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Moq;
using IVY.Objets;

namespace IVYTests
{
    /// <summary>
    /// Classe de tests concernant les membres.
    /// </summary>
    [TestClass]
    public class TestsMembres
    {
        #region Tests sur ObtenirMembres()

        /// <summary>
        /// Teste que la méthode retourne la liste des membres.
        /// </summary>
        [TestMethod]
        public void TestObtenirMembresSucces()
        {
            try
            {
                IGuildUser utilisateur1 = new Mock<IGuildUser>().Object;
                IGuildUser utilisateur2 = new Mock<IGuildUser>().Object;
                Membre membre = new();
                membre.AjouterMembre(utilisateur1);
                membre.AjouterMembre(utilisateur2);
                Assert.IsTrue(membre.ObtenirMembres().Count == 2);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la méthode est initialement à 0 et non nulle.
        /// </summary>
        [TestMethod]
        public void TestObtenirMembresInitaleVide()
        {
            try
            {
                Membre membre = new();
                Assert.IsTrue(membre.ObtenirMembres().Count == 0);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur ObtenirMembre()

        /// <summary>
        /// Teste que la méthode retourne le bon membre.
        /// </summary>
        [TestMethod]
        public void TestObtenirMembreSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Membre membre = new();
                membre.AjouterMembre(utilisateur);
                Assert.IsTrue(membre.ObtenirMembre(utilisateur.Id) == utilisateur);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode pour une entrée nulle.
        /// </summary>
        [TestMethod]
        public void TestObtenirMembreEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Membre membre = new();
                Assert.IsTrue(membre.ObtenirMembre(utilisateur.Id) == null);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode pour une entrée nulle.
        /// </summary>
        [TestMethod]
        public void TestObtenirMembreNull()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Membre membre = new();
                int? x = null;
                Action action = () => membre.ObtenirMembre((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion 

        #region Tests sur AjouterMembre()

        /// <summary>
        /// Teste que la méthode ajoute bel et bien le membre.
        /// </summary>
        [TestMethod]
        public void TestAjouterMembreSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Membre membre = new();
                membre.AjouterMembre(utilisateur);
                Assert.IsTrue(membre.ObtenirMembres().Count == 1);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur RetirerMembre()

        /// <summary>
        /// Teste que la méthode retire bel et bien le membre.
        /// </summary>
        [TestMethod]
        public void TestRetirerMembreSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Membre membre = new();
                membre.AjouterMembre(utilisateur);
                Assert.IsTrue(membre.ObtenirMembres().Count == 1);
                membre.RetirerMembre(utilisateur);
                Assert.IsTrue(membre.ObtenirMembres().Count == 0);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur SuspendreMembre()

        /// <summary>
        /// Teste que le membre est bel et bien suspendu.
        /// </summary>
        [TestMethod]
        public void TestSuspendreMembreSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Membre membre = new();
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode face à une entrée invalide.
        /// </summary>
        [TestMethod]
        public void TestSuspendreMembreEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Membre membre = new();
                Action action = () => membre.SuspendreMembre(utilisateur.Id);
                Assert.ThrowsException<NullReferenceException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode pour une entrée nulle.
        /// </summary>
        [TestMethod]
        public void TestSuspendreMembreNull()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Membre membre = new();
                int? x = null;
                Action action = () => membre.SuspendreMembre((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

    }
}
