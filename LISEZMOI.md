# IVYProject

## Description du projet
IVY est un bot de modération codé en C# qui utilise un système de pointage basé sur les réactions apposées aux messages. En effet, IVY observer le nombre de réactions d'un message afin de déterminer si son auteur a posé une action à récompenser ou plutôt à réprimander. IVY réprimande ultimement un utilisateur avec une suspension de 24 heures.

Ce programme est un échantillon pour un projet potentiel: il est donc très basique et n'est pas conçu pour être utilisé autrement qu'à des fins personnelles pour une petite communauté par exemple.

## Fonctionnalités
- Un utilisateur débute avec 5 chances au démarrage d’IVY.
- Un utilisateur en exclusion au démarrage d’IVY reçoit 1 chance seulement.
- Le pointage des utilisateurs se réinitialise à chaque redémarrage d’IVY.
- Le propriétaire de la guilde* et les bots ne sont pas modérés par IVY.
- Le propriétaire de la guilde* n’est pas privé par IVY de son droit de lever une suspension.
- Personne ne peut voir les pointages (à l'exception de celui qui lance le programme).
- Un utilisateur supprimant son message n’affecte pas son pointage.
- Un utilisateur reçois une plainte lorsqu’il reçois l’emoji 🚩 sur son message.
- Un utilisateur dépose une plainte contre un message lorsqu’il lui appose l’emoji 🚩.
- Un utilisateur reçois une appréciation lorsqu’il reçoit l’emoji ❤️ sur son message.
- Un utilisateur offre une appréciation d’un message lorsqu’il appose l’emoji ❤️.
- Le propriétaire de la guilde* peut lui aussi porter plainte et offrir des appréciations.
- Un utilisateur perd une chance dès que l’un de ses messages obtient 5 plaintes.
- Un utilisateur gagne une chance dès que l’un de ses messages obtient 5 appréciations.
- Un message ne peut faire perdre plus d’une seule chance à son auteur.
- Un message ne peut faire gagner plus d’une seule chance à son auteur.
- L’utilisateur n’ayant plus de chances restantes se voit exclu pour 24 heures.
- Lorsqu’un utilisateur termine sa période d’exclusion, celui-ci reçoit 1 chance seulement.
- Lorsqu’un utilisateur est exclu, tous les messages qui lui ont coûté une chance depuis le lancement d’IVY sont supprimés.
- Lorsqu’un utilisateur est banni de la guilde, celui-ci est complètement retiré du système de pointage.
- Lorsqu'un utilisateur est banni ou suspendu de la guilde, et ce, peut importe par qui, IVY envoie une annonce afin de rappeler la règle d'or du respect.
- Lorsqu'un utilisateur éligible au pointage effectue une action ou est modéré pour mauvais comportement alors qu'il n'est pas inclu dans le système, IVY l'ajoute automatiquement.

*Guilde est l'équivalent de serveur: en anglais, Discord.Net appelle les serveurs "Guilds" (guildes).

## Installation & Préparation
Voici les installations/préparations nécessaires pour travailler avec le projet IVY:
- Microsoft Visual Studio (2019 ou 2022 Community)
- Package NuGet Discord.Net 3.3.2 ou version plus récente (pour le projet IVY)
- Package NuGet coverlet.collector 3.1.2 ou version plus récente (pour le projet IVYTests)
- Package NuGet Microsoft.NET.Test.Sdk 17.1.0 ou version plus récente (pour le projet IVYTests)
- Package NuGet MSTest.TestAdapter 2.2.8 ou version plus récente (pour le projet IVYTests)
- Package NuGet MSTest.TestFramework 2.2.8 ou version plus récente (pour le projet IVYTests)
- PAckage NuGet Moq par Daniel Cazzulino, kzu, 4.17.2 ou version plus récente (pour le projet IVYTests)
- Application de bureau Discord pour PC avec le mode développeur activé
- Guilde/Serveur Discord avec le statut de propriétaire
- Bot créé depuis Discord Developer Portal avec les propriétés suivantes:
    - Activer `Presence Intent` et `Server Members Intent` (Bots/Privileged Gateway Intents)
    - Format d'URL de redirection avec la terminaison suivante (OAuth2/General/Redirects): `&permissions=1110249122816&scope=bot%20applications.commands`
- Fichier C:\TokenBot.txt créé et contenant uniquement le token du bot

## Utilisation
Une fois les installations complétées, invitez IVY sur l'un de vos serveurs puis lancer le programme depuis Visual Studio et laissez IVY faire sa magie!

## Tests
Une fois qu'IVY est associée à l'un de vos serveurs, ouvrez Visual Studio et faites un clique-droit n'importe où dans le projet pour obtenir l'action "Exécuter les tests".

Pour créer des tests, créer une nouvelle classe 

**Limitations et avertissements:**
- IVY n'est faite que pour être utilisée individuellement: ne l'invitez/l'utilisez que sur un seul serveur car le programme retournera une erreur à son lancement.
- Comme ce projet n'est qu'un "échantillon", IVY est hébergée de façon interne sur PC. (_self-hosting_)

## Contribution
Bien que ce projet n'est qu'un échantillon, il est possible que certains souhaitent y contribuer et il y a donc des exigences à respecter:

**Documentation et mise en forme du code**
- Utilisation du CamelCase pour le nom de variables, d’attributs, de constantes, de paramètres, de classes et de méthodes.
- Les noms des variables, attributs, constantes, paramètres, méthodes et classes devraient être en français sauf dans des cas exceptionnels. (Librairies anglophones, `Main`, `Async` etc.) 
- Les classes, les méthodes, les attributs et les constantes débutent par des majuscules.
- Les variables et les paramètres débutent par des minuscules.
- Les accolades sont toujours seules sur leur propre ligne.
- Les lignes de code ont un longueur maximale acceptée entre 80 et 90 caractères.
- Les noms des méthodes des tests devraient contenir le nom de la méthode testée si possible.
- Les noms des classes des tests devraient débuter pas "Tests" puis être complétés de façon significative.
- Espacements entre les éléments comme : 
    - `if (x != 1 && y > 5) `
    - `int x = 1 + chiffre`;
- L’utilisation des commentaires `//` et `/**/` est autorisée dans les méthodes.
- Les lignes ne devraient pas dépasser les 80 à 90 caractères.
- La documentation des classes, méthodes et attributs se fait avec le patron suivant :
    - `/// <summary>`
    - `/// Donne l’explication de l’attribut, la constante, la méthode ou la classe.`
    - `/// </summary>`
    - `/// <param name=""> Si la méthode/classe prend un ou des paramètres (faire une balise par paramètre). </param>`
    - `/// <return> Si la méthode/classe retourne un élément. </return>`

**Respect**

Ce projet est un bot de modération: montrez l'exemple et restez respectueux! Merci de respecter le but ultime de ce programme, sa configuration nécessaire, ses limites en tant qu'échantillon et ses fonctionnalités primaires. 😊

## License
MIT License

Copyright (c) 2022 Evelyne Jutras

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

